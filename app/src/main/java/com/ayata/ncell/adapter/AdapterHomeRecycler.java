package com.ayata.ncell.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.ncell.R;
import com.ayata.ncell.model.ModelHomeRecycler;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterHomeRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ModelHomeRecycler> listitem;

    public AdapterHomeRecycler(Context context, List<ModelHomeRecycler> listitem) {
        this.context = context;
        this.listitem = listitem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.recycler_home,parent,false);
        return (RecyclerView.ViewHolder)new ModelViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        //((ModelViewHolder)holder).imageView.setImageResource(listitem.get(position).getImage());

        Glide.with(context).load(listitem.get(position).getImage()).into(((ModelViewHolder)holder).imageView);

        ((ModelViewHolder)holder).datetime.setText(listitem.get(position).getDatetime());
        ((ModelViewHolder)holder).fund.setText(listitem.get(position).getFund());


    }

    @Override
    public int getItemCount() {
        return listitem.size();
    }

    public class ModelViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView datetime,fund;


        public ModelViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView= itemView.findViewById(R.id.home_card_image);
            datetime= itemView.findViewById(R.id.home_datetime);
            fund= itemView.findViewById(R.id.home_fund);

        }
    }
}
