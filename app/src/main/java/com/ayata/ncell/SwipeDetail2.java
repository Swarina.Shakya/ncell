package com.ayata.ncell;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ayata.ncell.fragment.FragmentSwipeDetail1;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SwipeDetail2 extends AppCompatActivity implements View.OnClickListener {

    TextView headerText,number_price,available_balance;
    int number=100;
    int number2=500;

    FloatingActionButton floatingActionButton,floatingActionButton2;

    Button submit_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_detail2);

        floatingActionButton=findViewById(R.id.swipe_detail2_btn_minus);
        floatingActionButton2=findViewById(R.id.swipe_detail2_btn_add);
        View headerLayout= findViewById(R.id.include_appbar);
        headerText= (TextView)headerLayout.findViewById(R.id.appbar_text_header);
        headerText.setText("TOPUP");
        number_price=findViewById(R.id.swipe_detail2_price);
        number_price.setText("Rs. "+String.valueOf(number));
        floatingActionButton.setOnClickListener(this);
        floatingActionButton2.setOnClickListener(this);

        submit_btn= findViewById(R.id.swipe_detail2_btn);
        submit_btn.setOnClickListener(this);

        ImageView cross= headerLayout.findViewById(R.id.appbar_cross);


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent= new Intent(SwipeDetail2.this, Home.class);
//                startActivity(intent);
//                finishAffinity();

                ((Home)getApplicationContext()).changeFragment(new FragmentSwipeDetail1());
            }
        });

        available_balance=findViewById(R.id.swipe_detail2_available_balance);
        available_balance.setText("Rs. "+String.valueOf(number2));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.swipe_detail2_btn_minus:
                if(number!=0) {
                    number = number - 1;
                    number2 = number2 + 1;
                    number_price.setText("Rs. " + String.valueOf(number));
                    available_balance.setText("Rs. " + String.valueOf(number2));
                }
                break;
            case R.id.swipe_detail2_btn_add:
                if(number2!=0) {
                    number = number + 1;
                    number2 = number2 - 1;
                    number_price.setText("Rs. " + String.valueOf(number));
                    available_balance.setText("Rs. " + String.valueOf(number2));
                }
                break;
            case R.id.swipe_detail2_btn:
                Intent intent =new Intent(SwipeDetail2.this,Swipe.class );
                startActivity(intent);
                finish();
                break;
        }
    }
}
