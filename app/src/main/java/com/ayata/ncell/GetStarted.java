package com.ayata.ncell;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ayata.ncell.adapter.AdapterGetStartedViewPager;
import com.ayata.ncell.fragment.FragmentGetStarted1;
import com.ayata.ncell.fragment.FragmentGetStarted2;
import com.ayata.ncell.fragment.FragmentGetStarted3;

import java.util.ArrayList;
import java.util.List;

public class GetStarted extends AppCompatActivity {

    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_started);

        button= findViewById(R.id.get_started_btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetStarted.this,Login.class);
                startActivity(intent);
                finishAffinity();
            }
        });

        List<Fragment> fragmentList= new ArrayList<>();
        fragmentList.add(new FragmentGetStarted1());
        fragmentList.add(new FragmentGetStarted2());
        fragmentList.add(new FragmentGetStarted3());

        viewPager= findViewById(R.id.get_started_viewpager);
        pagerAdapter= new AdapterGetStartedViewPager(getSupportFragmentManager(),fragmentList);
        viewPager.setAdapter(pagerAdapter);

    }
}
