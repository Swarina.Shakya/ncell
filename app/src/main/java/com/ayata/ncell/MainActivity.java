package com.ayata.ncell;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ayata.ncell.adapter.AdapterGetStartedViewPager;
import com.ayata.ncell.fragment.FragmentGetStarted1;
import com.ayata.ncell.fragment.FragmentGetStarted2;
import com.ayata.ncell.fragment.FragmentGetStarted3;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button= findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainActivity.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
