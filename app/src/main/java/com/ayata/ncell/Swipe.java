package com.ayata.ncell;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ayata.ncell.fragment.FragmentSwipeDetail1;

public class Swipe extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe);

        View toolbar = findViewById(R.id.swipe_appbar);
        ImageView cross= toolbar.findViewById(R.id.appbar_cross);


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent= new Intent(Swipe.this, Home.class);
//                startActivity(intent);
//                finishAffinity();

                ((Home)getApplicationContext()).changeFragment(new FragmentSwipeDetail1());
            }
        });

    }
}
