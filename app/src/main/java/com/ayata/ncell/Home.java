package com.ayata.ncell;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.ayata.ncell.fragment.FragmentHome;
import com.ayata.ncell.fragment.FragmentSwipeDetail1;

import com.google.android.material.bottomnavigation.BottomNavigationView;


public class Home extends  AppCompatActivity{


    private ImageView cross;
    private TextView toolbar_text;
    private BottomNavigationView bottomNavigationView;

    private FrameLayout frameLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        View toolbar= findViewById(R.id.home_appbar);
        cross= toolbar.findViewById(R.id.appbar_cross);
        cross.setVisibility(View.GONE);

        toolbar_text=toolbar.findViewById(R.id.appbar_text_header);
        toolbar_text.setText("Home");


        checkIfFragmentisNull();

        bottomNavigationView = findViewById(R.id.home_bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                    switch (menuItem.getItemId()) {
                        case R.id.nav_home:
                            //samepage
                            toolbar_text.setText("Home");
                            cross.setVisibility(View.GONE);
                            changeFragment(new FragmentHome());
                            break;

                        case R.id.nav_add:
                            toolbar_text.setText("Ncell Card");
                            cross.setVisibility(View.GONE);
                            changeFragment(new FragmentSwipeDetail1());
                            break;

                        case R.id.nav_more:
                            //add more
                            changeFragment(new FragmentHome());

                            break;


                    }
                    return true;

                }

            };

    public void changeFragment(Fragment selectedFragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment,selectedFragment).addToBackStack(null).commit();
//        if(onNavbottom)
//            bottomNavigationView.setVisibility(View.VISIBLE);
//        if(!onNavbottom)
//            bottomNavigationView.setVisibility(View.GONE);
    }

    public void checkIfFragmentisNull(){
        if (getSupportFragmentManager().findFragmentById(R.id.home_fragment)!= null){
            return;
        }
        FragmentHome fragmentHome= new FragmentHome();
        getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment,fragmentHome).addToBackStack(null).commit();
    }

    public void setCrossVisibility(Boolean crossVisibility){
        if(crossVisibility==true) {
            cross.setVisibility(View.VISIBLE);

        }
        if(crossVisibility==false){
            cross.setVisibility(View.GONE);
        }
    }
}
