package com.ayata.ncell.model;

public class ModelHomeRecycler {

    private String datetime , fund;

    private String imageUrl;

    private int image;



    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }



    public ModelHomeRecycler(String datetime, String fund, String imageUrl) {
        this.datetime = datetime;
        this.fund = fund;
        this.imageUrl = imageUrl;
    }


    public ModelHomeRecycler(String datetime, String fund, int image) {
        this.datetime = datetime;
        this.fund = fund;
        this.image = image;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFund() {
        return fund;
    }

    public void setFund(String fund) {
        this.fund = fund;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
