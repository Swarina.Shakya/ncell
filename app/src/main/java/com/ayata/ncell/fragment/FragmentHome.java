package com.ayata.ncell.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayata.ncell.R;
import com.ayata.ncell.adapter.AdapterHomeRecycler;
import com.ayata.ncell.model.ModelHomeRecycler;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private AdapterHomeRecycler adapterHome;
    private List<ModelHomeRecycler> recyclerList;


    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = view.findViewById(R.id.home_recycler);
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerList = new ArrayList<>();
        adapterHome = new AdapterHomeRecycler(getContext(), recyclerList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterHome);

        dataprepare();

        return view;
    }

    private void dataprepare() {
        recyclerList.add(new ModelHomeRecycler("Today 11:20 p.m.", "Rs. 100", R.drawable.creditcard));
        recyclerList.add(new ModelHomeRecycler("Today 11:20 p.m.", "Rs. 100", R.drawable.creditcard));
        recyclerList.add(new ModelHomeRecycler("Today 11:20 p.m.", "Rs. 100", R.drawable.creditcard));
    }

}
