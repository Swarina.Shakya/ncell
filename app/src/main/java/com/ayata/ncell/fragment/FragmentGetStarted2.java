package com.ayata.ncell.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayata.ncell.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGetStarted2 extends Fragment {


    public FragmentGetStarted2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_get_started2, container, false);
    }

}
