package com.ayata.ncell.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayata.ncell.Home;
import com.ayata.ncell.R;
import com.ayata.ncell.SwipeDetail2;
import com.ayata.ncell.adapter.AdapterHomeRecycler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSwipeDetail1 extends Fragment {


    FloatingActionButton send, receive;

    public FragmentSwipeDetail1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_swipe_detail1, container, false);

        send= view.findViewById(R.id.swipe_detail1_send);
        receive= view.findViewById(R.id.swipe_detail1_receive);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SwipeDetail2.class);
                startActivity(intent);
            }
        });
        return view;
    }

}
